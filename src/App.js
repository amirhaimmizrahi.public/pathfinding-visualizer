import { useState } from "react";
import Grid from "./components/grid/Grid";
import Bar from "./components/ui/Bar/Bar";
import GridProvider from "./contexts/GridContext";
import CustomThemeProvider from "./contexts/ThemeContext";
import MouseInputsProvider from "./contexts/MouseInputsContext";

function App() {
  const GRID_WIDTH = 73;
  const GRID_HEIGHT = 34;
  const [startPosition, setStartPosition] = useState([10, 15]);
  const [finishPosition, setFinishPosition] = useState([60, 15]);

  return (
    <CustomThemeProvider>
      <GridProvider
        width={GRID_WIDTH}
        height={GRID_HEIGHT}
        startPosition={startPosition}
        finishPosition={finishPosition}
      >
        <MouseInputsProvider>
          <Bar start={startPosition} finish={finishPosition} />
          <Grid setStart={setStartPosition} setFinish={setFinishPosition} />
        </MouseInputsProvider>
      </GridProvider>
    </CustomThemeProvider>
  );
}

export default App;
