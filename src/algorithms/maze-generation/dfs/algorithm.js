import {
  getUnvisitedNeighbors,
  randomNumber,
  removeWallBetweenNodes,
} from "../../../utils/maze-utils";
import { prepareGridToMazeGeneration } from "./utils";

export function mazeDfs(grid, enableDiagnals) {
  prepareGridToMazeGeneration(grid);

  let startNode =
    grid[randomNumber(0, grid.length)][randomNumber(0, grid[0].length)];

  while (startNode.position[0] % 2 || startNode.position[1] % 2) {
    startNode =
      grid[randomNumber(0, grid.length)][randomNumber(0, grid[0].length)];
  }

  const visitedNodes = [startNode];
  const clearPath = [];

  startNode.state.isVisited = true;

  let nearWall = null;

  while (visitedNodes.length !== 0) {
    let currentNode = visitedNodes.pop();
    clearPath.push(currentNode);
    if (currentNode.nearWall) clearPath.push(currentNode.nearWall);

    const unvisitedNeighbors = getUnvisitedNeighbors(
      grid,
      currentNode,
      enableDiagnals
    );

    if (unvisitedNeighbors.length > 0) {
      visitedNodes.push(currentNode);

      const randomNeighbor =
        unvisitedNeighbors[randomNumber(0, unvisitedNeighbors.length)];

      const removedWall = removeWallBetweenNodes(
        grid,
        currentNode,
        randomNeighbor
      );
      currentNode.nearWall = removedWall;
      removedWall.nearWall = nearWall;
      clearPath.push(removedWall);

      randomNeighbor.state.isVisited = true;
      visitedNodes.push(randomNeighbor);
    } else {
      clearPath.push(currentNode);
    }
    nearWall = currentNode;
  }
  for (const node of clearPath) {
    node.state.isVisited = false;
  }
  return clearPath;
}
