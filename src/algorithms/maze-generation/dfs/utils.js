export function prepareGridToMazeGeneration(grid) {
    for (const col of grid) {
      for (const node of col) {
        if (node.state.isStart || node.state.isFinish) continue;
        if (node.position[0] % 2 || node.position[1] % 2)
          node.state.isWall = true;
      }
    }
  }