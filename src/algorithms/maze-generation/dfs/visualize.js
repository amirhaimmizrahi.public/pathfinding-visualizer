import { nodeTheme } from "../../../contexts/ThemeContext";

export function mazeDfsVisualize(grid, clearPath, speed) {

  let index = 0;
  for (const x in grid) {
    for (const y in grid[x]) {
      index++;
      setTimeout(() => {
        const node = grid[x][y];
        if (node.state.isStart || node.state.isFinish) return;
        node.ref.current.className = `${nodeTheme} wall`;
      }, index / 2);
    }
  }

  const animatedNodes = [];

  for (let i in clearPath) {
    setTimeout(() => {
      const node = clearPath[i];

      if (node.state.isStart || node.state.isFinish) return;

      let isAnimated = false;

      for (const animatedNode of animatedNodes) {
        if (
          animatedNode.position[0] === node.position[0] &&
          animatedNode.position[1] === node.position[1]
        ) {
          isAnimated = true;
          break;
        }
      }

      if (!isAnimated) {
        node.ref.current.className = `${nodeTheme} mazeLeader`;
        animatedNodes.push(node);
      } else {
        node.ref.current.className = `${nodeTheme} mazeBacktrack`
      }
    }, (10 / speed) * i + index / 2);
  }
}
