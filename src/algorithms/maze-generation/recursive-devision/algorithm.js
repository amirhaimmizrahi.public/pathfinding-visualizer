import { devideChamber, fillChamber, getSubChamber } from "./utils";

export function mazeRecursiveDevision(grid) {
    const chambers = [grid]
    
    while (chambers.length !== 0) {
        const chamber = chambers.pop()

        if (chamber === null) continue

        const subChambers = devideChamber(chamber)

        if (subChambers === null) continue

        chambers.push(subChambers[0])
        chambers.push(subChambers[1])
    }
}