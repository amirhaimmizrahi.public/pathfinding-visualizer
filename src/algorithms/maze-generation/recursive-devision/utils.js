import { nodeTheme } from "../../../contexts/ThemeContext";
import { randomNumber } from "../../../utils/maze-utils";

export function devideChamber(chamber) {
  const width = chamber.length;
  const height = chamber[0].length;

  if (randomNumber(0, 2)) {
    return devideHorizontally(chamber);
  } else {
    return devideVertically(chamber);
  }
}

function devideHorizontally(chamber) {
  const width = chamber.length;
  const height = chamber[0].length;

  const devisionPoint = horizontalDevisionPoint(chamber);

  for (let y = 0; y < height; y++) {
    const node = chamber[devisionPoint][y];

    if (node.state.isStart || node.state.isFinish) continue;

    node.state.isWall = true;
    node.ref.current.className = `${nodeTheme} wall`;
  }

  const holeNode =
    chamber[devisionPoint][horizontalHolePoint(chamber, devisionPoint)];
  holeNode.state.isWall = false;
  holeNode.ref.current.className = `${nodeTheme} `;

  const leftChamber =
    devisionPoint <= 1 || height <= 1
      ? null
      : getSubChamber(chamber, [0, 0], devisionPoint, height);
  const rightChamber =
    width - devisionPoint - 1 <= 1 || height <= 1
      ? null
      : getSubChamber(
          chamber,
          [devisionPoint + 1, 0],
          width - devisionPoint - 1,
          height
        );

  return [leftChamber, rightChamber];
}

function devideVertically(chamber) {
  const width = chamber.length;
  const height = chamber[0].length;

  const devisionPoint = verticalDevisionPoint(chamber);

  for (let x = 0; x < width; x++) {
    const node = chamber[x][devisionPoint];

    if (node.state.isStart || node.state.isFinish) continue;

    node.state.isWall = true;
    node.ref.current.className = `${nodeTheme} wall`;
  }

  const holeNode =
    chamber[verticalHolePoint(chamber, devisionPoint)][devisionPoint];
  holeNode.state.isWall = false;
  holeNode.ref.current.className = `${nodeTheme}`;

  const topChamber =
    width <= 1 || devisionPoint <= 1
      ? null
      : getSubChamber(chamber, [0, 0], width, devisionPoint);
  const bottomChamber =
    width <= 1 || height - devisionPoint - 1 <= 1
      ? null
      : getSubChamber(
          chamber,
          [0, devisionPoint + 1],
          width,
          height - devisionPoint - 1
        );

  return [topChamber, bottomChamber];
}

function verticalDevisionPoint(chamber) {
  const height = chamber[0].length;
  let devisionPoint = null;
  let node = null;

  do {
    devisionPoint = randomNumber(0, height);
    node = chamber[0][devisionPoint];
  } while (node.position[1] % 2 === 1);

  return devisionPoint;
}

function horizontalDevisionPoint(chamber) {
  const width = chamber.length;
  let devisionPoint = null;
  let node = null;

  do {
    devisionPoint = randomNumber(0, width);
    node = chamber[devisionPoint][0];
  } while (node.position[0] % 2 === 1);

  return devisionPoint;
}

function verticalHolePoint(chamber, devisionPoint) {
  const width = chamber.length;
  let holePoint = null;
  let node = null;

  do {
    holePoint = randomNumber(0, width);
    node = chamber[holePoint][devisionPoint];
  } while (
    node.position[0] % 2 === 0 ||
    node.state.isStart ||
    node.state.isFinish
  );

  return holePoint;
}

function horizontalHolePoint(chamber, devisionPoint) {
  const height = chamber[0].length;
  let holePoint = null;
  let node = null;

  do {
    holePoint = randomNumber(0, height);
    node = chamber[devisionPoint][holePoint];
  } while (
    node.position[1] % 2 === 0 ||
    node.state.isStart ||
    node.state.isFinish
  );

  return holePoint;
}

function getSubChamber(chamber, topLeft, width, height) {
  const subChamber = [];

  for (let x = topLeft[0]; x < topLeft[0] + width; x++) {
    const col = chamber[x];
    subChamber.push(col.slice(topLeft[1], topLeft[1] + height));
  }

  return subChamber;
}

function fillChamber(chamber) {
  for (const col of chamber) {
    for (const node of col) {
      node.ref.current.className = `${nodeTheme} path`;
    }
  }
}
