import { PriorityQueue } from "@datastructures-js/priority-queue";
import {
  calculateDistance,
  getUnvisitedNeighbors,
} from "../../utils/pathfinding-utils";
import { applyPayload, gridToArray } from "../../utils/grid-utils";

export function astar(grid, startPosition, finishPosition, enableDiagnals) {
  applyPayload(grid, {
    fDistance: Infinity,
    gDistance: 0,
    hDistance: 0,
    previousNode: null,
  });

  const visitedNodes = [];
  const unvisitedNodes = PriorityQueue.fromArray(gridToArray(grid), (a, b) => {
    {
      const payloadA = a.payload;
      const payloadB = b.payload;

      if (payloadA.fDistance < payloadB.fDistance) return -1;
      if (payloadA.fDistance > payloadB.fDistance) return 1;

      if (a.position[0] < b.position[0]) return -1;
      if (a.position[0] > b.position[0]) return 1;

      if (a.position[1] < b.position[1]) return -1;
      if (a.position[1] > b.position[1]) return 1;
    }
  });

  updateQueueNodeDistance(
    unvisitedNodes,
    startPosition,
    0,
    calculateHueristicDistance(
      grid[startPosition[0]][startPosition[1]],
      finishPosition,
      enableDiagnals
    )
  );

  while (!unvisitedNodes.isEmpty()) {
    let currentNode = unvisitedNodes.dequeue();

    //break if got to finish node
    if (
      currentNode.position[0] === finishPosition[0] &&
      currentNode.position[1] === finishPosition[1]
    )
      break;

    //if got to a node with distance infinity, that means we're stuck and there is no path
    if (currentNode.payload.fDistance === Infinity) {
      return [[], visitedNodes];
    }

    const neighbors = getUnvisitedNeighbors(grid, currentNode, enableDiagnals);

    for (const neighbor of neighbors) {
      const newGDistance =
        currentNode.payload.gDistance +
        calculateDistance(neighbor, currentNode, enableDiagnals);
      const newHDistance = calculateHueristicDistance(
        neighbor,
        finishPosition,
        enableDiagnals
      );

      if (newGDistance + newHDistance < neighbor.payload.fDistance) {
        updateQueueNodeDistance(
          unvisitedNodes,
          neighbor.position,
          newGDistance,
          newHDistance
        );
        neighbor.payload.previousNode = currentNode;
      }
    }

    currentNode.state.isVisited = true;
    visitedNodes.push(currentNode);
  }

  return [findShortestPath(grid, finishPosition), visitedNodes];
}

function findShortestPath(grid, finishPosition) {
  const path = [];
  let currentNode = grid[finishPosition[0]][finishPosition[1]];

  while (currentNode.payload.previousNode !== null) {
    path.push(currentNode);
    currentNode = currentNode.payload.previousNode;
  }

  return path.reverse();
}

function updateQueueNodeDistance(queue, nodePosition, g, h) {
  const node = queue.remove(
    (node) =>
      node.position[0] === nodePosition[0] &&
      node.position[1] === nodePosition[1]
  )[0];
  node.payload.fDistance = g + h;
  node.payload.gDistance = g;
  node.payload.hDistance = h;
  queue.enqueue(node);
}

function calculateHueristicDistance(node, finishPosition, enableDiagnals) {
  const dx = Math.abs(finishPosition[0] - node.position[0]) + 0.0;
  const dy = Math.abs(finishPosition[1] - node.position[1]) + 0.0;

  if (!enableDiagnals) return dx + dy;

  const dMax = Math.max(dx, dy);
  const dMin = Math.min(dx, dy);

  return Math.sqrt(2.0) * dMin + dMax - dMin;
}
