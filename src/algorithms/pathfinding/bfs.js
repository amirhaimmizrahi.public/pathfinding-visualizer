import { getUnvisitedNeighbors } from "../../utils/pathfinding-utils";
import { applyPayload } from "../../utils/grid-utils";

export function bfs(grid, startPosition, finishPosition, enableDiagnals) {
  applyPayload(grid, { previousNode: null });

  const visitedNodes = [];
  const unvisitedNodes = [grid[startPosition[0]][startPosition[1]]];

  while (unvisitedNodes.length !== 0) {
    let currentNode = unvisitedNodes.shift();

    //break if got to finish node
    if (
      currentNode.position[0] === finishPosition[0] &&
      currentNode.position[1] === finishPosition[1]
    )
      break;

    const neighbors = getUnvisitedNeighbors(grid, currentNode, enableDiagnals);

    for (const neighbor of neighbors) {
      neighbor.state.isVisited = true;
      unvisitedNodes.push(neighbor);
      neighbor.payload.previousNode = currentNode;
    }

    currentNode.state.isVisited = true;
    visitedNodes.push(currentNode);
  }
  return [findShortestPath(grid, finishPosition), visitedNodes];
}

function findShortestPath(grid, finishPosition) {
  const path = [];
  let currentNode = grid[finishPosition[0]][finishPosition[1]];

  while (currentNode.payload.previousNode !== null) {
    path.push(currentNode);
    currentNode = currentNode.payload.previousNode;
  }

  return path.reverse();
}
