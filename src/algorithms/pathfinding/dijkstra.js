import { PriorityQueue } from "@datastructures-js/priority-queue";
import {
  calculateDistance,
  getUnvisitedNeighbors,
} from "../../utils/pathfinding-utils";
import { applyPayload, gridToArray } from "../../utils/grid-utils";

export function dijkstra(grid, startPosition, finishPosition, enableDiagnals) {
  applyPayload(grid, { distance: Infinity, previousNode: null });

  const visitedNodes = [];
  const unvisitedNodes = PriorityQueue.fromArray(gridToArray(grid), (a, b) => {
    {
      const da = a.payload.distance;
      const db = b.payload.distance;

      if (da < db) return -1;
      if (da > db) return 1;

      if (a.position[0] < b.position[0]) return -1;
      if (a.position[0] > b.position[0]) return 1;

      if (a.position[1] < b.position[1]) return -1;
      if (a.position[1] > b.position[1]) return 1;
    }
  });

  updateQueueNodeDistance(unvisitedNodes, startPosition, 0);

  while (!unvisitedNodes.isEmpty()) {
    let currentNode = unvisitedNodes.dequeue();

    //break if got to finish node
    if (
      currentNode.position[0] === finishPosition[0] &&
      currentNode.position[1] === finishPosition[1]
    )
      break;

    //if got to a node with distance infinity, that means we're stuck and there is no path
    if (currentNode.payload.distance === Infinity) {
      return [[], visitedNodes];
    }

    const neighbors = getUnvisitedNeighbors(grid, currentNode, enableDiagnals);

    for (const neighbor of neighbors) {
      const newDistance =
        currentNode.payload.distance +
        calculateDistance(neighbor, currentNode, enableDiagnals);

      if (newDistance < neighbor.payload.distance) {
        updateQueueNodeDistance(unvisitedNodes, neighbor.position, newDistance);
        neighbor.payload.previousNode = currentNode;
      }
    }

    currentNode.state.isVisited = true;
    visitedNodes.push(currentNode);
  }

  return [findShortestPath(grid, finishPosition), visitedNodes];
}

function findShortestPath(grid, finishPosition) {
  const path = [];
  let currentNode = grid[finishPosition[0]][finishPosition[1]];

  while (currentNode.payload.previousNode !== null) {
    path.push(currentNode);
    currentNode = currentNode.payload.previousNode;
  }

  return path.reverse();
}

function updateQueueNodeDistance(queue, nodePosition, distance) {
  const node = queue.remove(
    (node) =>
      node.position[0] === nodePosition[0] &&
      node.position[1] === nodePosition[1]
  )[0];
  node.payload.distance = distance;
  queue.enqueue(node);
}
