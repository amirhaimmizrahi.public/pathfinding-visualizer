import { nodeTheme } from "../../contexts/ThemeContext";

export function visualize(path, visitedNodes, speed, isInstant = false) {
  const visitedSpeed = 3;
  const pathSpeed = 20;
  const pathDelay = 1300;

  visitedNodes.shift(); //ignore start node
  path = path.slice(0, -1);

  if (isInstant) {
    visualizeInstantly(path, visitedNodes);
  } else {
    for (const i in visitedNodes) {
      setTimeout(() => {
        visitedNodes[i].ref.current.className = `${nodeTheme} animateVisited`;
      }, visitedSpeed * i * (5 / speed));
    }
    for (const i in path) {
      setTimeout(() => {
        path[i].ref.current.className = `${nodeTheme} animatePath`;
      }, pathSpeed * i * (2 / speed) + visitedSpeed * visitedNodes.length * (5 / speed) + pathDelay);
    }
  }
}

function visualizeInstantly(path, visitedNodes) {

  for (const i in visitedNodes) {
    visitedNodes[i].ref.current.className = `${nodeTheme} visited`;
  }
  for (const i in path) {
    path[i].ref.current.className = `${nodeTheme} path`;
  }
}
