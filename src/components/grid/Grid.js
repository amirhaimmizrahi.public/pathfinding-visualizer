import { Stack } from "@mui/material";
import React from "react";
import Node from "./Node";
import { useGrid } from "../../contexts/GridContext";

export default function Grid({ setStart, setFinish }) {
  const [grid, ] = useGrid()

  return (
    <Stack direction="row">
      {grid.map((col, x) => (
        <Stack key={x} direction="column">
          {col.map((node, y) => (
            <Node
              key={y}
              node={node}
              setStart={setStart}
              setFinish={setFinish}
            />
          ))}
        </Stack>
      ))}
    </Stack>
  );
}
