import React, { useEffect } from "react";
import "./Node.css";
import "./NodeDark.css";
import {
  makeFinish,
  makeNormal,
  makeStart,
  makeWall,
  unmakeWall,
} from "../../utils/node-utils";
import { useMouseInputs } from "../../contexts/MouseInputsContext";
import {nodeTheme} from '../../contexts/ThemeContext'

export default function Node({ node, setStart, setFinish }) {
  const MouseInputs = useMouseInputs();

  useEffect(() => {
    let className = `${nodeTheme} `;

    if (node.state.isVisited) className += "visited";
    else if (node.state.isStart) className += "start";
    else if (node.state.isFinish) className += "finish";
    else if (node.state.isWall) className += "wall";
    else if (node.state.isPath) className += "path";

    node.ref.current.className = className;
  }, [node.ref]);

  function onMouseMoveHandler() {
    if (!MouseInputs.isMouseDown) return;
    if (MouseInputs.isDragging) return;
    if (node.state.isStart || node.state.isFinish) return;

    if (MouseInputs.isRightToggled) {
      if (node.state.isWall) unmakeWall(node);
    } else makeWall(node);
  }

  function onMouseEnterHandler() {
    if (!MouseInputs.isDragging) return;
    if (node.state.isStart || node.state.isFinish) return;

    node.state.wasWall = node.state.isWall;

    if (MouseInputs.dragData.isStart) {
      makeStart(node);
      setStart(node.position);
    } else {
      makeFinish(node);
      setFinish(node.position);
    }
  }

  function onMouseLeaveHandler() {
    if (!MouseInputs.isDragging) return;

    if (MouseInputs.dragData.isStart) {
      if (node.state.isFinish) return;
    } else {
      if (node.state.isStart) return;
    }

    if (node.state.wasWall) makeWall(node);
    else makeNormal(node);
  }

  function onMouseDownHandler() {
    if (node.state.isStart || node.state.isFinish) {
      MouseInputs.isDragging = true;

      MouseInputs.dragData = {
        isStart: node.state.isStart,
      };
    }
  }

  function handleAnimationEnd() {
    if (node.ref.current.className === `${nodeTheme} mazeLeader` || node.ref.current.className === `${nodeTheme} mazeBacktrack`) {
      node.ref.current.className = nodeTheme
    }
  }

  return (
    <div
      className={`${nodeTheme}`}
      onMouseMove={onMouseMoveHandler}
      onMouseEnter={onMouseEnterHandler}
      onMouseLeave={onMouseLeaveHandler}
      onMouseDown={onMouseDownHandler}
    >
      <div ref={node.ref} onAnimationEnd={handleAnimationEnd}></div>
    </div>
  );
}
