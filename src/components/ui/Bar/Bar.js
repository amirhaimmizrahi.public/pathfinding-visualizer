import { AppBar, Stack } from "@mui/material";
import React, { useState } from "react";
import SelectPathfindingAlgorithm from "./pathfinding-section/SelectPathfindingAlgorithm";
import VisualizeButton from "./pathfinding-section/VisualizeButton";
import UnvisualizeButton from "./UnvisualizeButton";
import ClearButton from "./ClearButton";
import SpeedSlider from "./SpeedSlider";
import EnableDiagnalsSwitch from "./EnableDiagnalsSwitch";
import MazeGenerationSection from "./maze-generation-section/MazeGenerationSection";
import PathfindingSection from "./pathfinding-section/PathfindingSection";
import SelectTheme from "./SelectTheme";

export default function Bar({ start, finish }) {
  
  const [visualizationSpeed, setVisualizationSpeed] = useState(4);
  

  return (
    <AppBar position="static">
      <Stack
        direction="row"
        spacing="100px"
        sx={{
          width: "100%",
          height: "90px",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <PathfindingSection start={start} finish={finish} visualizationSpeed={visualizationSpeed}/>
        <MazeGenerationSection visualizationSpeed={visualizationSpeed}/>
        <Stack
          direction="row"
          spacing="30px"
          sx={{
            display: "flex",
            flex: 1,
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <SpeedSlider
            speed={visualizationSpeed}
            setSpeed={setVisualizationSpeed}
          />
          <ClearButton />
          <UnvisualizeButton />
          <SelectTheme/>
        </Stack>
      </Stack>
    </AppBar>
  );
}
