import { Button } from "@mui/material";
import React from "react";
import { useGrid } from "../../../contexts/GridContext";

export default function ClearButton() {
  const [, clear] = useGrid();

  return (
    <Button
      color="inherit"
      onClick={() => {
        clear();
      }}
    >
      clear board
    </Button>
  );
}
