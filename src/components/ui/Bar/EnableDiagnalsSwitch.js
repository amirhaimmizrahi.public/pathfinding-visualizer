import { Stack, Switch, Typography } from "@mui/material";
import React from "react";

export default function EnableDiagnalsSwitch({
  enableDiagnals,
  setEnableDiagnals,
}) {
  return (
    <Stack direction="row" sx={{ alignItems: "center" }}>
      <Typography sx={{ color: "inherit" }}>Enable Diagnals: </Typography>
      <Switch
        value={enableDiagnals}
        onChange={() => {
          setEnableDiagnals(!enableDiagnals);
        }}
      />
    </Stack>
  );
}
