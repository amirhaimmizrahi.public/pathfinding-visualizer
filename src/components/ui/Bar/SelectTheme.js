import { FormControl, InputLabel, MenuItem, Select } from "@mui/material";
import React from "react";
import { nodeTheme, changeNodeTheme } from "../../../contexts/ThemeContext";

export default function SelectTheme() {
  return (
    <FormControl sx={{ minWidth: "20%" }}>
      <InputLabel sx={{ color: "inherit" }}>Theme</InputLabel>
      <Select
        sx={{ color: "inherit" }}
        value={nodeTheme}
        onChange={(e) => {
        //   changeNodeTheme(e.target.value);
        }}
      >
        <MenuItem value={"nodeDark"}>Dark</MenuItem>
        <MenuItem value={"node"}>Normal</MenuItem>
      </Select>
    </FormControl>
  );
}
