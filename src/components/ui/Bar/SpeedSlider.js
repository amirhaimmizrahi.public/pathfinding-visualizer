import { FormControl, FormLabel, Slider } from "@mui/material";
import React from "react";

export default function SpeedSlider({ speed, setSpeed }) {
  return (
    <FormControl sx={{ width: "25%", alignItems: "center" }}>
      <FormLabel sx={{ color: "inherit" }}>Visualization Speed</FormLabel>
      <Slider
        sx={{ color: "inherit" }}
        size="small"
        value={speed}
        onChange={(e, value) => {
          setSpeed(value);
        }}
        step={1}
        min={1}
        max={10}
        getAriaValueText={(value) => `${value}`}
        valueLabelDisplay="auto"
      />
    </FormControl>
  );
}
