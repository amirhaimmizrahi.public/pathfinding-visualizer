import { Button } from "@mui/material";
import React from "react";
import { useGrid } from "../../../contexts/GridContext";
import { unvisualizeGrid } from "../../../utils/grid-utils";

export default function UnvisualizeButton() {
  const [grid] = useGrid();

  return (
    <Button
      color="inherit"
      onClick={() => {
        unvisualizeGrid(grid);
      }}
    >
      un-visualize
    </Button>
  );
}
