import React from "react";
import { Button } from "@mui/material";
import { useGrid } from "../../../../contexts/GridContext";
import { mazeDfs } from "../../../../algorithms/maze-generation/dfs/algorithm";
import { mazeDfsVisualize } from "../../../../algorithms/maze-generation/dfs/visualize";
import { mazeRecursiveDevision } from "../../../../algorithms/maze-generation/recursive-devision/algorithm";

export default function GenerateButton({
  algorithm,
  visualizationSpeed,
  enableDiagnals,
}) {
  const [grid] = useGrid();

  return (
    <Button
      sx={{ backgroundColor: "" }}
      variant="contained"
      onClick={() => {
        if (algorithm === "dfs") {
          const clearPath = mazeDfs(grid, enableDiagnals);
          mazeDfsVisualize(grid, clearPath, visualizationSpeed);
        }
        else if (algorithm === "recursive-devision") {
          mazeRecursiveDevision(grid)
        }

      }}
    >
      generate maze
    </Button>
  );
}
