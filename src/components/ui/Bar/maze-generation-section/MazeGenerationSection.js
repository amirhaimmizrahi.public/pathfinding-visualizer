import { Stack } from "@mui/material";
import React, { useState } from "react";
import SelectMazeAlgorithm from "./SelectMazeAlgorithm";
import GenerateButton from "./GenerateButton";
import EnableDiagnalsSwitch from "../EnableDiagnalsSwitch";

export default function MazeGenerationSection({ visualizationSpeed }) {
  const [enableDiagnals, setEnableDiagnals] = useState(false);
  const [algorithm, setAlgorithm] = useState("recursive-devision");

  return (
    <Stack
      direction="row"
      spacing="30px"
      sx={{
        display: "flex",
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <SelectMazeAlgorithm
        algorithm={algorithm}
        setAlgorithm={setAlgorithm}
      />
      <Stack direction="column">
        <GenerateButton
          algorithm={algorithm}
          visualizationSpeed={visualizationSpeed}
          enableDiagnals={enableDiagnals}
        />
        {(algorithm === "dfs") && <EnableDiagnalsSwitch
          enableDiagnals={enableDiagnals}
          setEnableDiagnals={setEnableDiagnals}
        />}
        
      </Stack>
    </Stack>
  );
}
