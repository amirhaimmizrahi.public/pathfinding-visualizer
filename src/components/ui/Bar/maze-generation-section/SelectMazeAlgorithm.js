import { FormControl, InputLabel, MenuItem, Select } from "@mui/material";
import React from "react";

export default function SelectMazeAlgorithm({ algorithm, setAlgorithm }) {
  return (
    <FormControl sx={{ minWidth: "1%" }}>
      <InputLabel sx={{ color: "inherit" }}>Maze Algorithm</InputLabel>
      <Select
        sx={{ color: "inherit" }}
        value={algorithm}
        onChange={(e) => {
          setAlgorithm(e.target.value);
        }}
      >
        <MenuItem value={"dfs"}>Depth First Search</MenuItem>
        <MenuItem value={"recursive-devision"}>Recursive Devision</MenuItem>
      </Select>
    </FormControl>
  );
}
