import { Stack } from "@mui/material";
import React, { useState } from "react";
import SelectPathfindingAlgorithm from "./SelectPathfindingAlgorithm";
import VisualizeButton from "./VisualizeButton";
import EnableDiagnalsSwitch from "../EnableDiagnalsSwitch";

export default function PathfindingSection({start, finish, visualizationSpeed}) {
  const [algorithm, setAlgorithm] = useState("dijkstra");
  const [enableDiagnals, setEnableDiagnals] = useState(false);

  return (
    <Stack
      direction="row"
      spacing="30px"
      sx={{
        display: "flex",
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <SelectPathfindingAlgorithm
        algorithm={algorithm}
        setAlgorithm={setAlgorithm}
      />
      <Stack direction="column">
        <VisualizeButton
          start={start}
          finish={finish}
          algorithm={algorithm}
          visualizationSpeed={visualizationSpeed}
          enableDiagnals={enableDiagnals}
        />
        <EnableDiagnalsSwitch
          enableDiagnals={enableDiagnals}
          setEnableDiagnals={setEnableDiagnals}
        />
      </Stack>
    </Stack>
  );
}
