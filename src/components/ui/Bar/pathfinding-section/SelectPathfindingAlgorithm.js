import { FormControl, InputLabel, MenuItem, Select } from "@mui/material";
import React from "react";

export default function SelectPathfindingAlgorithm({ algorithm, setAlgorithm }) {
  return (
    <FormControl sx={{ minWidth: "26%" }}>
      <InputLabel sx={{ color: "inherit" }}>Pathfinding Algorithm</InputLabel>
      <Select
        sx={{ color: "inherit" }}
        value={algorithm}
        onChange={(e) => {
          setAlgorithm(e.target.value);
        }}
      >
        <MenuItem value={"dijkstra"}>Dijkstra</MenuItem>
        <MenuItem value={"astar"}>A*</MenuItem>
        <MenuItem value={"bfs"}>Breadth First Search</MenuItem>
        <MenuItem value={"dfs"}>Depth First Search</MenuItem>
      </Select>
    </FormControl>
  );
}
