import React from "react";
import { Button } from "@mui/material";
import { visualize } from "../../../../algorithms/pathfinding/visualize";
import { useGrid } from "../../../../contexts/GridContext";
import { unvisualizeGrid } from "../../../../utils/grid-utils";
import { dijkstra } from "../../../../algorithms/pathfinding/dijkstra";
import { bfs } from "../../../../algorithms/pathfinding/bfs";
import { dfs } from "../../../../algorithms/pathfinding/dfs";
import { astar } from "../../../../algorithms/pathfinding/astar";

const PATHFINDING_FUNCTION = {
  dijkstra: dijkstra,
  bfs: bfs,
  dfs: dfs,
  astar: astar,
};

export default function VisualizeButton({
  start,
  finish,
  algorithm,
  visualizationSpeed,
  enableDiagnals,
}) {
  const [grid, clear] = useGrid();

  return (
    <Button
      color="success"
      variant="contained"
      onClick={() => {
        unvisualizeGrid(grid);
        const [path, visitedNodes] = PATHFINDING_FUNCTION[algorithm](
          grid,
          start,
          finish,
          enableDiagnals
        );
        visualize(path, visitedNodes, visualizationSpeed);
      }}
    >
      visualize {algorithm}
    </Button>
  );
}
