import React, { createContext, useContext, useState } from "react";
import { createGrid } from "../utils/grid-utils";

const gridContext = createContext();

export function useGrid() {
  return useContext(gridContext);
}

export default function GridProvider({
  width,
  height,
  startPosition,
  finishPosition,
  children,
}) {
  const [grid, setGrid] = useState(
    createGrid(width, height, startPosition, finishPosition)
  );

  function clear() {
    setGrid(createGrid(width, height, startPosition, finishPosition));
  }

  return <gridContext.Provider value={[grid, clear]}>{children}</gridContext.Provider>;
}
