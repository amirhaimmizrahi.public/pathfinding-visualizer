import React, { createContext, useContext } from "react";

const mouseInputsContext = createContext();
const MouseInputs = {
  isMouseDown: false,
  isRightToggled: false,
  isDragging: false,
  dragData: null,
};

export function useMouseInputs() {
  return useContext(mouseInputsContext);
}

export default function MouseInputsProvider({ children }) {
  return (
    <mouseInputsContext.Provider value={MouseInputs}>
      <div
        onMouseDown={(e) => {
          MouseInputs.isMouseDown = true;
        }}
        onMouseUp={(e) => {
          MouseInputs.isRightClick = false;
          MouseInputs.isMouseDown = false;
          MouseInputs.isDragging = false;
          MouseInputs.dragData = null;
        }}
        onContextMenu={(e) => {
          e.preventDefault();
          MouseInputs.isRightToggled = !MouseInputs.isRightToggled;
          MouseInputs.isMouseDown = false;
          MouseInputs.isDragging = false;
          MouseInputs.dragData = null;
        }}
      >
        {children}
      </div>
    </mouseInputsContext.Provider>
  );
}
