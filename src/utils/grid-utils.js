import { createRef } from "react";
import { nodeTheme } from "../contexts/ThemeContext";

export function applyPayload(grid, payload) {
  for (const col of grid) {
    for (const node of col) {
      node.payload = { ...payload };
    }
  }
}

export function gridToArray(grid) {
  const array = [];

  for (const col of grid) {
    for (const node of col) {
      array.push(node);
    }
  }

  return array;
}

export function createGrid(width, height, startPosition, finishPosition) {
  const grid = [];

  for (let x = 0; x < width; x++) {
    grid.push([]);

    for (let y = 0; y < height; y++) {
      grid[x].push(createNode(x, y));
    }
  }

  grid[startPosition[0]][startPosition[1]].state.isStart = true;
  grid[finishPosition[0]][finishPosition[1]].state.isFinish = true;

  return grid;
}

function createNode(x, y) {
  return {
    position: [x, y],
    ref: createRef(),
    state: {
      isWall: false,
      isStart: false,
      isFinish: false,
      isVisited: false,
      isPath: false,
    },
    payload: null,
  };
}

export function unvisualizeGrid(grid) {
  for (const col of grid) {
    for (const node of col) {
      node.state.isVisited = false;
      if (
        !node.state.isWall &&
        !node.state.isStart &&
        !node.state.isFinish
      ) {
        node.ref.current.className = `${nodeTheme}`;
      }
    }
  }
}