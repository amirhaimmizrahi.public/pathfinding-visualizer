export function getNeighbors(grid, node, enableDiagnals) {
  const [x, y] = node.position;
  const neighbors = [];

  if (x > 1) neighbors.push(grid[x - 2][y]);
  if (x < grid.length - 2) neighbors.push(grid[x + 2][y]);
  if (y > 1) neighbors.push(grid[x][y - 2]);
  if (y < grid[0].length - 2) neighbors.push(grid[x][y + 2]);

  if (enableDiagnals) {
    if (x > 1 && y > 1) neighbors.push(grid[x - 2][y - 2]);
    if (x < grid.length - 2 && y < grid[0].length - 2)
      neighbors.push(grid[x + 2][y + 2]);
    if (x < grid.length - 2 && y > 1) neighbors.push(grid[x + 2][y - 2]);
    if (x > 1 && y < grid[0].length - 2) neighbors.push(grid[x - 2][y + 2]);
  }

  return neighbors;
}

export function getUnvisitedNeighbors(grid, node, enableDiagnals) {
  const neighbors = getNeighbors(grid, node, enableDiagnals);

  return neighbors.filter((neighbor) => !neighbor.state.isVisited);
}

export function randomNumber(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

export function removeWallBetweenNodes(grid, nodeA, nodeB) {
  const x = (nodeA.position[0] + nodeB.position[0]) / 2;
  const y = (nodeA.position[1] + nodeB.position[1]) / 2;

  grid[x][y].state.isWall = false;
  return grid[x][y];
}
