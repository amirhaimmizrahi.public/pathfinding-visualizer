import { nodeTheme } from "../contexts/ThemeContext";

export function makeWall(node) {
  node.state.isWall = true;
  node.state.isStart = false;
  node.state.isFinish = false;
  node.ref.current.className = `${nodeTheme} wall`;
}

export function unmakeWall(node) {
  node.ref.current.className = `${nodeTheme} deletedWall`;
  node.state.isWall = false;
  node.state.isStart = false;
  node.state.isFinish = false;
}

export function makeNormal(node) {
  node.ref.current.className = `${nodeTheme}`;
  node.state.isWall = false;
  node.state.isStart = false;
  node.state.isFinish = false;
}

export function makeStart(node) {
  node.state.isStart = true;
  node.state.isWall = false;
  node.state.isFinish = false;

  node.ref.current.className = `${nodeTheme} start`;
}
export function makeFinish(node) {
  node.state.isStart = false;
  node.state.isWall = false;
  node.state.isFinish = true;

  node.ref.current.className = `${nodeTheme} finish`;
}
