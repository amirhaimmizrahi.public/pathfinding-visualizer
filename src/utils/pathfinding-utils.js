export function getNeighbors(grid, node, enableDiagnals) {
  const [x, y] = node.position;
  const neighbors = [];

  if (x > 0) neighbors.push(grid[x - 1][y]);
  if (x < grid.length - 1) neighbors.push(grid[x + 1][y]);
  if (y > 0) neighbors.push(grid[x][y - 1]);
  if (y < grid[0].length - 1) neighbors.push(grid[x][y + 1]);

  if (enableDiagnals) {
    if (x > 0 && y > 0) neighbors.push(grid[x - 1][y - 1]);
    if (x < grid.length - 1 && y < grid[0].length - 1)
      neighbors.push(grid[x + 1][y + 1]);
    if (x < grid.length - 1 && y > 0) neighbors.push(grid[x + 1][y - 1]);
    if (x > 0 && y < grid[0].length - 1) neighbors.push(grid[x - 1][y + 1]);
  }

  return neighbors;
}

export function getUnvisitedNeighbors(grid, node, enableDiagnals) {
  const neighbors = getNeighbors(grid, node, enableDiagnals);

  return neighbors.filter(
    (neighbor) => !neighbor.state.isVisited && !neighbor.state.isWall
  );
}

export function calculateDistance(nodeA, nodeB, enableDiagnals) {
  if (!enableDiagnals) return 1;

  const dx = Math.abs(nodeB.position[0] - nodeA.position[0]) + 0.0;
  const dy = Math.abs(nodeB.position[1] - nodeA.position[1]) + 0.0;

  return Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));
}
